<!DOCTYPE html>
<html>
    <head>
       @include('masterlayout.head')
    </head>    
    
    <body>
            <header>
                @include('masterlayout.header')
            </header>
        
            <div id="main">
 <section class="bg-primary" id="about">

      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading text-white">PENDIDIKAN AL-ITTIHAD</h2>
            <hr class="light my-4">
            <p class="text-faded mb-4">
              SMP Plus Al-Ittihad<br>
Terakreditasi A<br>
SK : 02.00/207/BAP-SM/SK/X/2012<br><br>

SMA Plus Al-Ittihad (Jurusan IPA & IPS)<br>
Terakreditasi A<br>
SK : 02.00/208/BAP-SM/SK/X/2012<br><br>

SMK Terpadu Al-Ittihad<br>
 Rekayasa Perangkat Lunak (RPL)<br>
SK : RPL (A) : 02.00/204/BAP-SM/SK/XII/2015<br><br>

 Analisis Pengujian Lab (Analisis Kimia 3 Tahun)<br>
SK : AK (A) : 02.00/204/BAP-SM/SK/XII/2015<br><br>

 Otomatisasi dan Tata Kelola Perkantoran / Administrasi Perkantoran : Terdaftar<br>
          </div>
        </div>
      </div>
    </section>

            </div>
    </body>
</html>