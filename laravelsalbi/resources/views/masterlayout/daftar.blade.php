<!DOCTYPE html>
<html>
    <head>
       @include('masterlayout.head')
    </head>    
    
    <body>
            <header>
                @include('masterlayout.header')
            </header>
        
            <div id="main">
 <section class="bg-primary" id="about">

      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading text-white">[ INFORMASI PPDB 2018 ]</h2>
            <hr class="light my-4">
            <p class="text-faded mb-4">
Pondok Pesantren Al-Ittihad Cianjur Menyelenggarakan :<br><br>
SMP Plus Al-Ittihad<br>
Terakreditasi A<br>
SK : 02.00/207/BAP-SM/SK/X/2012<br><br>

SMA Plus Al-Ittihad (Jurusan IPA & IPS)<br><br>
Terakreditasi A<br>
SK : 02.00/208/BAP-SM/SK/X/2012<br>

SMK Terpadu Al-Ittihad<br><br>
1. Rekayasa Perangkat Lunak (RPL)<br>
SK : RPL (A) : 02.00/204/BAP-SM/SK/XII/2015<br>

2. Analisis Pengujian Lab (Analisis Kimia 3 Tahun)<br><br>
SK : AK (A) : 02.00/204/BAP-SM/SK/XII/2015<br>

3. Otomatisasi dan Tata Kelola Perkantoran / Administrasi Perkantoran : Terdaftar<br><br>

Berbasis Pesantren dengan Prioritas Pendidikan :<br><br>

1. Kitab Kuning - Pendidikan Kitab Kuning dilaksanakan 3 kali dalam sehari.<br>
2. Bahasa Arab dan Inggris - Penggunaan Bahasa Arab dan Inggris sebagai Program Unggulan.<br>
3. Pengetahuan Umum dan Teknologi - Santri menempuh pendidikan formal di pesantren.<br>
4. Tahfidz Qur'an - Program untuk Santri yang berminat<br>

UNTUK PENDAFTARAN DI BUKA MULAI BULAN MARET S/D JUNI 2018<br>

Sekretariat Pondok Pesantren Al-Ittihad Cianjur, Jl. Raya Bandung KM 3, Desa Bojong, Kec. Karangtengah, Kab. Cianjur, 43281<br>

(Dari segala jurusan turun di Terminal Rawabango Cianjur kurang lebih 50 m ke Pesantren, depan Pom Bensin Rawabango )<br>

Website : www.al-ittihad.or.id<br>
Facebook : Pondok Pesantren Al-Ittihad Cianjur<br>
Instagram : @infoalittihad<br>

Contact Person :<br>
1. Ust Mimid Mahmudin, S.Pd. (085798887408)<br>
2. Ust. Dede Ahmad S, S.Pd. (087720697987)<br><br>

Bagi yang membutukan brosur bisa japri via WA 085314124502.</p><br>
            <a class="btn btn-light btn-xl js-scroll-trigger" href="daftar">Daftar</a>
          </div>
        </div>
      </div>
    </section>

            </div>
    </body>
</html>