@extends('frontend.index') 
@section('content')
    <header class="masthead text-center text-white d-flex">
      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h1 class="text-uppercase">
              <strong>AYO MONDOK <br>GAK MONDOK GAK KEREN</strong>
            </h1>
            <hr>
          </div>
          <div class="col-lg-8 mx-auto">
            <p class="text-faded mb-5">"Give us your child and we'll give his future"</p>
            <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Jelajahi</a>
          </div>
        </div>
      </div>
    </header>

    <section class="bg-primary" id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading text-white">Visi & Misi</h2>
            <hr class="light my-4">
            <p class="text-faded mb-4">

    Visi<br>Mencetak insan religius berwawasan global yang menguasai ilmu agama, pengetahuan umum dan teknologi<br> Misi<br>Menjadikan SMP, SMA Plus dan SMK Terpadu Pondok Pesantren Al-Ittihad sebagai wahana pembinaan cendikiawan muslim kaffah
</p>
            
          </div>
        </div>
      </div>
    </section>

    <section id="services">
      
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <i class="fa fa-4x fa-diamond text-primary mb-3 sr-icons"></i>
              <h3 class="mb-3">Berfikir Cerdas</h3>
              
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <i class="fa fa-4x fa-paper-plane text-primary mb-3 sr-icons"></i>
              <h3 class="mb-3">Man Jadda Wa Jada</h3>
              
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <i class="fa fa-4x fa-newspaper-o text-primary mb-3 sr-icons"></i>
              <h3 class="mb-3">Bekerja Keras</h3>
              
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <i class="fa fa-4x fa-heart text-primary mb-3 sr-icons"></i>
              <h3 class="mb-3">Fastabiqul Khoirot</h3>
              
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="p-0" id="portfolio">
    <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">SEKOLAH KAMI</h2>
            <hr class="my-4">
          </div>
        </div>
      </div>
      <div class="container-fluid p-0">
        <div class="row no-gutters popup-gallery">
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/1.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/1.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    PHOTO
                  </div>
                  <div class="project-name">
                    LOGO SMP AL-ITTIHAD
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/2.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/2.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                   PHOTO
                  </div>
                  <div class="project-name">
                    LOGO SMA AL-ITTIHAD
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/3.png">
              <img class="img-fluid" src="img/portfolio/thumbnails/3.png" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    PHOTO
                  </div>
                  <div class="project-name">
                    LOGO SMK AL-ITTIHAD
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/4.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/4.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    PHOTO
                  </div>
                  <div class="project-name">
                    SMP AL-ITTIHAD
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/5.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/5.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    PHOTO
                  </div>
                  <div class="project-name">
                    SMA AL-ITTIHAD
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/6.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/6.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    PHOTO
                  </div>
                  <div class="project-name">
                    SMK AL-ITTIHAD
                  </div>
                </div>
              </div>
            </a>
          </div>
        </div>
      </div>
    </section>

    <section class="bg-dark text-white">
      <div class="container text-center">
        <h2 class="mb-4">Jelajahi Kami</h2>
        <a class="btn btn-light btn-xl sr-button" href="http://al-ittihad.or.id/">Open Now!</a>
      </div>
    </section>
    @endsection