<!DOCTYPE html>
<html>
    <head>
       @include('masterlayout.head')
    </head>    
    
    <body>
            <header>
                @include('masterlayout.header')
            </header>
        
            <div id="main">
 <section class="bg-primary" id="about">

      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading text-white">KEGIATAN AL-ITTIHAD</h2>
            <hr class="light my-4">
            <p class="text-faded mb-4">
              
PASKIBRA AL-ITTIHAD<br><br>
1.  LATAR BELAKANG<br><br>
Dalam kegiatan sekolah terkadang sering kita dengar nama paskibra, sebetulnya apa yang dimaksud paskibra? Paskibra adalah suatu organisasi yang terdiri atas pemuda pemudi terpilih yang bertugas menaikan dan menurunkan bendera dengan memakai pakaian putih-putih sehingga terlihat gagah.
Dalam paskibra tidak hanya melatih baris-berbaris tetapi mengarahkannya kepada pendidikan kepemimpinan kehidupan sehari-hari. Seperti cara berkenalan, bertemu, menelpon, sejarah bangsa, bahkan mengarahkan kepada pergaulan antar lawan jenis agar tidak salah jalan dan berjalan positif menurut syariat islam.<br><br>
2.  MAKSUD<br><br>
Paskibra didirikan dengan maksud :<br>
1.  Pembuat Karya<br>
2.  Pelopor Perjuangan Bangsa<br>
3.  Pemacu Semangat<br>
4.  Pertahanan Negara<br>
5.  Pembentukan persatuan dan kesatuan<br><br>
3.  TUJUAN<br><br>
• Membentuk pemuda yang bermental baik<br>
• Membentuk persaudaraan antara pemuda<br>
• Menjadi pandu ibu pertiwi yang korsa :kreatif, bertata karma, disiplin, kritis, tidak mudah putus asa, berani dan semangat.
Dengan adanya paskibra banyak tujuan untuk membangun sikap diri dalam kehidupan dengan mempunyai cita-cita mengibarkan bendera pusaka.<br><br><br>
         <a class="btn btn-light btn-xl js-scroll-trigger button_waterpump" href="ekstrau1">PRAMUKA</a>
         <a class="btn btn-light btn-xl js-scroll-trigger button_airpump" href="ekstrau2">PMR</a>
          </div>
        </div>
      </div>
    </section>

            </div>
    </body>
</html>