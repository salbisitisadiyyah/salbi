<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return View::make('masterlayout.content');
});

Route::get('about', function() {
  return View::make('masterlayout.about');  
});

Route::get('services', function() {
  return View::make('masterlayout.services');  
});

Route::get('galery', function() {
  return View::make('masterlayout.galery');  
});

Route::get('daftar', function() {
  return View::make('masterlayout.daftar');  
});

Route::get('didik', function() {
  return View::make('masterlayout.didik');  
});

Route::get('ekstrau', function() {
  return View::make('masterlayout.ekstrau');  
});

Route::get('ekstrau1', function() {
  return View::make('masterlayout.ekstrau1');  
});

Route::get('ekstrau2', function() {
  return View::make('masterlayout.ekstrau2');  
});

Route::get('ekstrap', function() {
  return View::make('masterlayout.ekstrap');  
});

Route::get('ekstrao', function() {
  return View::make('masterlayout.ekstrao');  
});